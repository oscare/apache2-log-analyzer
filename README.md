# Apache2 log analyzer

Automatización del análisis de bitacoras generadas por un servidor Web Apache. Se muestra una idea visual sobre las visitas recibidas en el servidor.