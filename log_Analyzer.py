#!usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT

#	Autores:
#		Espinosa Curiel Oscar
#		Ferrusca Jorge
#		Rodriguez Gallardo Pedro

"""
	Importamos la funcion para abrir archivos comprimidos
	Importamos la clase para obtener parametros como banderas
"""
from gzip import open as gopen
from optparse import OptionParser
from sys import exit
import re
from collections import Counter
from datetime import datetime
#importamos la libreria pyplo para poder Graficar
import matplotlib.pyplot as plt
import numpy as np

def addOptions():
	parser = OptionParser()
	parser.add_option('-I', '--input', 
		dest='input', default=None, 
		help='Files that will be analyzed. If there is more than one, they must be separated by commas'
	)
	parser.add_option('-v', '--verbose', 
		dest="verbose",
		action="store_const", const=True, 
		help='Print to stdout execution details'
	)
	parser.add_option('-u', '--useragent',
		dest='useragent', default=None, 
		help='Show the n top of user agents that made a request to the server'
	)
	# parser.add_option('-i', '--ipaddress',
	# 	dest='ips', default=None, 
	# 	help='Show the n top of IP address that made a request to the server'
	# )
	parser.add_option('-r', '--resources', 
		dest='resources', default=None,
		help='Show the n top of resources that were visited'
	)
	parser.add_option('-s', '--statusCodes', 
		dest='statusCodes', default=None, 
		help='Show top n of status codes by the server'
	)
	parser.add_option('-o', '--output', 
		dest='output', default=None, 
		help='Show the n top of responses gives by the server'
	)
	parser.add_option('-a', '--attack', 
		dest='attack', default=None, 
		help='Analyze if there was some attacks incidents on the server. format: num_requests/minutes'
	)
	parser.add_option('-c', '--configFile', 
		dest='configFile', default=None, 
		help='specify file with program options. If -c is set, other options are ignored'
	)
	parser.add_option('-i', '--IPs', dest='IPs', default=None, 
		help='Show the n top of IPs that visited the server'
	)	
	options, args = parser.parse_args()
	return options

def printResults(results, title):
	print "\n",title
	print results
	return [title, results]

def checkOptions(opts):
	results = []
	if opts.input == None:
		raise ValueError('Error 0: Debes ingresar por lo menos un archivo de bitácora')
	if (not opts.useragent and not opts.resources and not opts.statusCodes and not opts.attack):
		raise ValueError('Error n: Debes ingresar por lo menos un tipo de análisis')
	files = splitInput(opts.input)
	for file in files:
		if isGzipFile(file):
			fileOld = file
			file = decompress(fileOld)
		""" ------------------ Bandera -a   ----------"""
		if opts.attack:
			datos = opts.attack.split('/')
			if isAValidNumber(datos[0]) and isAValidNumber(datos[1]):
				IPdic = genIPdic(getAllGETrequests(file))
				r1 = searchAttacks(int(datos[0]),int(datos[1]),IPdic, opts.verbose)
				if r1: 
					r1 = printResults(r1, 'Potenciales IP\'s atacantes (ip, numero de solicitudes en ' + datos[1] + ' minuto(s)):')
					results.append(r1)
					# PlotTop(getTop(5, result))
				else:
					print "\t\nNo se registraron ataques al servidor\n"
			else:
				raise ValueError('Error 4: Los valores dados a la bandera -a no son numeros validos')

		"""" ----------------- Bandera -r   ---------------"""
		#Obtenemos el n top de los recursos solicitados
		if opts.resources:
			if isAValidNumber(opts.resources):
				r2 = getTop(int(opts.resources), getAllResources(file, opts.verbose))
				PlotTop(r2, u'Recursos más solicitados')
				r2 = printResults(r2, 'Recursos más solicitados { Recurso: número de veces }:')
				results.append(r2)
			else:
				raise ValueError('Error 1: El valor dado en la bandera -r no es un numero valido')

		""" ------------------ Bandera -s  -------------------"""
		#Obtenemos el n top de las respuestas mas frecuentes
		if opts.statusCodes:
			if isAValidNumber(opts.statusCodes):
				r3 = getTop(int(opts.statusCodes), getAllResponses(file, opts.verbose))
				PlotTop(r3, u'Respuestas más frecuentes')
				r3 = printResults(r3, 'Respuestas más fecuentes del servidor { Código de respuesta: numero de veces }:')
				results.append(r3)
			else:
				raise ValueError('Error 2: El valor dado en la bandera -s no es un numero valido')

		""" -------------------- Bandera -u   ------------------"""
		#Obtenemos el n top de los agentes de usuario
		if opts.useragent:
			if isAValidNumber(opts.useragent):
				r4 = getTop(int(opts.useragent), getAllUserAgents(file, opts.verbose))
				PlotTop(r4, u'Top de Agentes de usuario')
				r4 = printResults(r4, 'Tipos de agente más utilizado { Nombre de agente: número de veces }:')
				results.append(r4)
			else:
				raise ValueError('Error 3: El valor dado en la bandera -u no es un numero valido')
		""" -------------------- Bandera -i   ------------------"""
		#Obtenemos el n top de las IPs de que solicitan el servidor
		if opts.IPs:
			if isAValidNumber(opts.IPs):
				r5 = getTop(int(opts.IPs), getTopIPs(file, opts.verbose))
				PlotTop(r5, u'IP\'s con más peticiones')
				r5 = printResults(r5, 'IP\'s con más peticiones { ip: numero de solicitudes }:')
				results.append(r5)
			else:
				raise ValueError('Error 5: El valor dado en la bandera -i no es un numero valido')
		if opts.output:
			writeFile(opts.output, results)

def writeFile(name, results):
	try:
		file = open(name, 'w')
		file.write('Reporte de análisis\n\n')
		for result in results:
			for r in result:
				file.write(str(r))
				file.write("\n")
			file.write("\n------------\n")
		file.close()
	except IOError:
		raise ValueError('No se pudo escribir el archivo de reporte')
	except Exception as e:
		print e
		raise ValueError('Error. Verifique la sintaxis de las opciones dentro del archivo')


def isGzipFile(binnacle):
	"""
		Funcion que valida si el agumento dado es o no un archivo comprimido
		gzfile. Se hace comparando el magic number de gzip con los primeros
		cuatro numeros hexadecimales del archivo.
	"""
	magic_header = '\x1f\x8b'
	#Abrimos el archivo en modo lectura binaria
	with open(binnacle, 'rb') as gFile:
		startFile = gFile.read(len(magic_header))
		if startFile == magic_header:
			return True

	return False

def splitInput(inputFiles):
	"""
		Funcion que separa en una lista los archivos dados como argumento
	"""
	return inputFiles.split(',')

def isAValidNumber(number):
	"""
		Funcion que valida que los argumentos que se espera sean numeros
		sean numeros mayores que cero.
	"""
	if number.isdigit():
		if int(number) > 0:
			return True
	return False


def getTopIPs(logfile, verbose):
	#if options.IPs is not None:
	CuentaIP = []
	patron_IP = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}'

	with open(logfile,'r') as f:
		log = f.read()
		mi_listaIP = re.findall(patron_IP,log)
		if verbose: print mi_listaIP
		CuentaIP = Counter(mi_listaIP)

	
	'''
	ips = CuentaIP.items()
	ips.sort(key=lambda x: x[1], reverse=True)
	#plot
	PlotTop(ips[:int(options.IPs)])
	'''		
	return CuentaIP

def PlotTop(top_tupla, title):
	'''
	Funcion que Grafica las n Top entradas de cada uno los parametros solicitados
	'''
	#Separa en etiquetas y valores contenidos en las tuplas de cada Top
	labels, values = zip(*top_tupla)		
	indexes = np.arange(len(labels))
	width = 0.5
	plt.figure()	
	plt.title(title)
	#Parametros de la grafica
	plt.bar(indexes, values, width)
	plt.xticks(indexes + width , labels)
				

def getAllResources(inputFile, verbose):
	"""
		Funcion que revisa todos los recursos visitados y sus incidencias.
		Almacena los valores en un diccionario de la forma:
		'recurso': numero_incidencias
		Devuelve el diccionario generado
	"""
	resourcesDic = {}
	with open(inputFile, 'r') as fileR:
		#Saltamos la primera linea porque es una linea vacia
		fileR.readline()
		for line in fileR:
			#Separamos los datos leidos y almacenamos unicamente la posicion en $
			#que se encuentra el metodo http y recurso solicitado
			request = line.split('"')[1]
			if len(request) > 1 :
				resource = request.split(' ')[1]
				if verbose: print resource
				#Aumentamos la incidencia del recurso solicitado o lo agregamos al
				#diccionario en caso que no se encuentre con una incidencia como val$
				if resource in resourcesDic:
					resourcesDic[resource] += 1
				else:
					resourcesDic[resource] = 1

	return resourcesDic

def getTop(n, valuesDic):
	"""
		Funcion que obtiene los n valores mas grandes de un diccionario.
		Es usada para obtener los n recursos mas visitados y las n respuestas
		mas frecuentes del servidor.
		Se copian los elementos del diccionario a una lista y se ordena
		a partir del valor (que es el numero de incidencias de cada recurso)
		se ordena de manera descendiente
		Retorna una lista con los n recursos mas visitados
	"""
	valuesList = valuesDic.items()
	valuesList.sort(key=lambda x: x[1], reverse=True)
	return valuesList[:n]

def getAllResponses(inputFile, verbose):
	"""
		Funcion que obtiene todas las incidencias de las respuestas dadas
		por el servidor.
		De cada log en el archivo se obtiene exactamente el codigo de respuesta
		dado y se agrega en un diccionario como llave, en el que se va
		incrementando un contador que es el valor de esa llave.
		Devuelve el diccionario con  todas las respuestas y sus incidencias
	"""
	responsesDic = {}
	with open(inputFile, 'r') as fileR:
		#Saltamos la primera linea porque es una linea vacia
		fileR.readline()
		for line in fileR:
			#Separamos cada log por comillas
			request = line.split('"')
			#encontramos exactamente el codigo de respuesta del servidor
			response = request[2][1:4]
			if verbose: print response
			#Se aumenta el contador de incidencias por cada respuesta
			if response in responsesDic:
				responsesDic[response] += 1
			else:
				responsesDic[response] = 1

	return responsesDic

def getAllUserAgents(inputFile, verbose):
	"""
		Funcion que obtiene todos los agentes de usuario que han hecho una peticion
		al servidor con sus incidencias. Se excluye a la cadena vacia que puede
		encontrarse.
		Devuelve un diccionario con el agente de usuario como llave y el total de
		incidencias contadas como valor.
	"""
	userAgentsDic = {}
	with open(inputFile, 'r') as fileU:
		fileU.readline()
		for line in fileU:
			regexAgent = r'"[a-zA-Z]+/[0-9]{1,2}\.[0-9]{1,3}.*"'
			userAgent = re.findall(regexAgent, line)
			if userAgent:
				userAgent = userAgent[0].split('"')[1]
				if verbose: print userAgent
				#Obtenemos el parametro donde se encuentra el agente de usuario
				# request = line.split('"')
				# userAgent = request[5]
				#Excluimos la cadena vacia del diccionario y el - que son valores puestos
				#En el metodo POST. Se excluye Go-http-client porque no es un 
				#agente de usuario valido
				# if userAgent != "" and userAgent != '-' and userAgent != 'Go-http-client/1.1':
				if userAgent != 'Go-http-client/1.1':
					if userAgent in userAgentsDic:
						userAgentsDic[userAgent] += 1
					else:
						userAgentsDic[userAgent] = 1
	return userAgentsDic

def getAllGETrequests(inputFile):
	"""
		Funcion que obtiene todos los request hechos con GET al servidor
		Retorna una lista de todos los request de GET
	"""
	getRequests = []
	with open(inputFile, 'r') as fileG:
		for line in fileG:
			#Buscamos como subcadena GET
			if 'GET' in line:
				getRequests.append(line)

	return getRequests

def getDateIP(requestString):
	"""
		Funcion que obtiene la IP y fecha de un request dado como parametro.
		Retorna una tupla con IP,fecha del request dado
	"""
	request = requestString.split('"')
	req = request[0].split(' ')
	#Parseamos a una variable de tipo datetime
	dateV = datetime.strptime(req[3][1:], '%d/%b/%Y:%H:%M:%S')

	#Retornamos (IP, fecha)
	return (req[0], dateV)

def genIPdic(getRequests):
	"""
		Funcion que genera un diccionario con todas las fechas en que una 
		IP hizo un request al servidor.
		La direccion IP es la llave del diccionario y una lista de todas sus
		fechas es el valor de la llave.
		Retorna el diccionario generado
		Para poder generar y agregar fechas a la lista como valor, fue necesario
		inicializar una lista para poder manipularla como tal
	"""
	IPdateDic = {}
	for req in getRequests:
		tup = getDateIP(req)
		if tup[0] in IPdateDic:
			#Obtenemos la lista definida como valor y le agregamos la nueva fecha
			lista = IPdateDic[tup[0]]
			lista.append(tup[1])
			#Sobreescribimos el valor con la nueva lista obtenida
			IPdateDic[tup[0]] = lista

		else:
			#Inicializamos una lista vacia para poder manipularla como lista
			lista = []
			lista.append(tup[1])
			#Almacenamos como valor esta lista con una fecha
			IPdateDic[tup[0]] = lista

	return IPdateDic

def getMinutesDiff(date1, date2):
	"""
		Devuelve la diferencia en minutos entre dos objetos de tipo DateTime
	"""
	return float((date2-date1).total_seconds() / 60)

def searchAttacks(numAtks, minutes, IPdateDic, verbose):
	"""
		Funcion que busca si hubo algun ataque al servidor de acuerdo al numero
		de ataques y minutos establecidos como parametros.
		Retorna una lista de tuplas con (IP,numero de ataques) de las IP que
		se consideraron como ataque
	"""
	attacksList = []
	#Se recorre el diccionario de IP y fechas de requests
	for ip, dates in IPdateDic.items():
		print ip, len(dates)
		#Si el numero de ataques es menor al numero de consultas que hizo una IP,
		#entonces se procede a analizar las consultas de esta IP, en caso contrario
		#no tiene caso analizarlo, ya que no cumple con la cantidad de consultas
		#suficientes para considerarlo ataque
		if numAtks <= len(dates):
			#Ordenamos la lista de consultas
			dates.sort()
			#Obtenemos los segundos de diferencia entre la consulta mas vieja y la
			#mas reciente que hizo una IP al servidor
			# diffM = (dates[-1] - dates[0]).total_seconds()
			#Si esta diferencia es menor al numero de minutos establecidos,
			#entonces este se considera ataque dado que tiene mas consultas que
			#el parametro y se hizo en menor tiempo que los minutos dados.
			# if int(diffM/60) < minutes:
			if getMinutesDiff(dates[0], dates[-1]) < minutes:
				#Se registra el ataque en una lista. Se guarda una tupla de 
				#(IP_atacante, numero_de_ataques_que_realizo)
				if verbose: print ip, dates
				attacksList.append((ip,len(dates)))
			else:
				#En caso de que este intervalo sea mayor, es necesario recorrer la
				#lista de consultas para ver si en algun momento se cumple que se
				#hayan hecho n consultas en m minutos dados.
				print "sillega"
				i = 0
				while(i < len(dates)):
					try:
						if(len(dates[i:]) > numAtks):
							# si hubo numAtks peticiones en un rango menor o igual a MINUTES, contabilizar dichas peticiones
							if(getMinutesDiff(dates[i], dates[i+numAtks-1]) <= minutes):
								requests = len(dates[i:i+numAtks])
								for j,d in enumerate(dates[i+numAtks:]):
									if (getMinutesDiff(dates[i], d) <= minutes):	
										requests += 1
										if j+1 == len(dates[i+numAtks:]):
											attacksList.append((ip, requests))
											i = i + requests  
											break	
									else:
										#seguiremos analizando, ahora a partir de 1 request despues de el del rango
										attacksList.append((ip, requests))
										i = i + requests  
										break	
							else:
								i += 1
						else:
							i = len(dates)
					except IndexError:
						raise ValueError('Hubo algun problema, intente de nuevo')
	return attacksList

def setOption(option, value):
	global opts
	if option == '-I' or option == '--input':
		opts.input = value.strip() 
	if option == '-v' or option == '--verbose':
		opts.verbose = value
	if option == '-u' or option == '--useragent':
		opts.useragent = value.strip()
	if option == '-r' or option == '--resources':
		opts.resources= value.strip()
	if option == '-s' or option == '--statusCodes':
		opts.statusCodes = value.strip()
	if option == '-o' or option == '--output':
		opts.output = value.strip()
	if option == '-a' or option == '--attack':
		opts.attack = value.strip()

def readConfigFile(name):
	print 'Leyendo opciones de: ',name
	try:
		file = open(name, 'r')
		lines = file.readlines()
		for line in lines:
			#ignorando las lineas que contengan #
			if '#' in line:
				continue
			if '-' in line:
				option = line.split('=')
				# if (len(option) < 2 and (option[0] == '-v' or option[0] == '--verbose')):
				if len(option) < 2:
					option.append(True)
				#se pone la bandera con el valor sin espacios en blanco
				# print(option[1])
				setOption(option[0], option[1])
	except IOError:
		raise ValueError('Error: posiblemente no exista el archivo')
	except Exception as e:
		# print e
		raise ValueError('Error. Verifique la sintaxis de las opciones dentro del archivo\n-opcion=valor')

def decompress(filename):
    fileD = filename[:filename.rfind(".gz")]
    with gopen(filename, 'r') as fr:
        with open(fileD, 'w') as fw:
            fw.write(fr.read())

    return fileD        

if __name__ == '__main__':
	try:
		opts = addOptions()
		if opts.configFile:
			readConfigFile(opts.configFile)
		checkOptions(opts)
		plt.show()
	except ValueError as ve:
		print (ve)
		exit(1)
